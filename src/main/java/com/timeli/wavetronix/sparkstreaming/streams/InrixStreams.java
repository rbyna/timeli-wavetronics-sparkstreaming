package com.timeli.wavetronix.sparkstreaming.streams;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.spark.MongoSpark;
import com.mongodb.spark.config.WriteConcernConfig;
import com.mongodb.spark.config.WriteConfig;
import com.timeli.wavetronix.sparkstreaming.job.JedisPoolHolder;
import com.timeli.wavetronix.sparkstreaming.lib.CsvToJsonConvertor;
import com.timeli.wavetronix.sparkstreaming.lib.MongoUpdate;
import com.timeli.wavetronix.sparkstreaming.misc.ExternalConfig;
import com.timeli.wavetronix.sparkstreaming.obj.Detectors;
import com.timeli.wavetronix.sparkstreaming.obj.Incident;
import com.timeli.wavetronix.sparkstreaming.obj.LocationData;
import com.timeli.wavetronix.sparkstreaming.utils.JedisUtils;
import com.timeli.wavetronix.sparkstreaming.utils.Utils;
import javafx.util.Pair;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.bson.Document;
import redis.clients.jedis.Jedis;
import scala.Tuple2;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class InrixStreams implements Serializable {

    public InrixStreams() {

    }

    private static HashMap<String, String> detectorToEventId = new HashMap<>();
    private static HashMap<String, Incident> detectorToIncident = new HashMap<>();
    private static Jedis jedis;
    private static MongoCollection<Document> inrixDataCollection;
    private static int id;
    private static List<String> detectors = new ArrayList<>();

    public static void initialize(ExternalConfig config) {
        //Opening the mongodb connection
        initializeMongodb(config);
        //When spark reloads, need to load the pr   e existing incidents from MongoDB and save into map
        loadCurrentIncidents(inrixDataCollection);
        //Load all the incidents that have occured one hour before current time
    }



    public static void main(String[] args) {
        Date date3 = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        System.out.println(df.format(date3));
    }

    private static void initializeMongodb(ExternalConfig config) {
        MongoClientOptions.Builder options_builder = new MongoClientOptions.Builder();
        options_builder.maxConnectionIdleTime(600);
        options_builder.socketKeepAlive(true);
        MongoClient mongo = new MongoClient(new MongoClientURI(config.getMongoConnectionString()));
        MongoDatabase mongodb = mongo.getDatabase(config.getMongoDbName());
        inrixDataCollection = mongodb.getCollection(config.getMongoCollectionNameForProcessedInrixData());
    }


    private static void loadCurrentIncidents(MongoCollection<Document> inrixDataCollection) {
        FindIterable<Document> currentIncidents = MongoUpdate.getCurrentIncidents(inrixDataCollection);
        for (Document doc : currentIncidents) {
            String detector_id = String.valueOf(doc.get("detector_Id"));
            String startTimestamp = String.valueOf(doc.get("start_timestamp"));
            String eventId = String.valueOf(doc.get("event_id"));
            Object secondary = doc.get("secondary");
            boolean duplicate = false;
            if (secondary != null) {
                duplicate = Boolean.valueOf(String.valueOf(secondary));
            }
            detectorToEventId.put(detector_id, eventId);
            detectorToIncident.put(detector_id, new Incident(startTimestamp, "IN_PROGRESS", 1, 4, 0, detector_id, eventId, null, duplicate));
        }
        System.out.println("Previous Incident loaded : " + detectorToIncident);
    }

    public static void processInrixStreams(JavaDStream<String> detectors, ExternalConfig config) {
        JedisPoolHolder.init("23.101.126.159", 6379);
        jedis = JedisPoolHolder.getInstance().getResource();
        jedis.auth("Clust3rax$");
        //convert the input inrix stream to object
        JavaDStream<Detectors> inrixDataObjStream = detectors.map(convertToDetectorsObject());
        //filter the interstate segments
        JavaDStream<Detectors> filteredIncidents = inrixDataObjStream.filter(filterDefectedSensors());
        FindIterable<Document> currentIncidents = MongoUpdate.getCurrentIncidents(inrixDataCollection);
        /*detectors.retainAll(Collections.EMPTY_LIST);
        for (Document doc : currentIncidents) {
            String code = String.valueOf(doc.get("code"));
            detectors.add(code);
            String key = code + "L";
            long count = jedis.llen(key);
            List<String> nearbyCodes = jedis.lrange(key, 0, count - 1);
            detectors.addAll(nearbyCodes);
        }

        JavaDStream<Detectors> filteredSegments = filteredIncidents.filter(filterOnlyIncidentSegments());
        //Pushing raw data to mongodb
        JavaDStream<Document> rawData = filteredSegments.map(convertRawDataToMongoDocument());
        saveIncidentsToMongo(rawData, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForRawInrixData(),
                scala.Option.apply(config.getMongoConnectionString()),
                false, 500, 0, WriteConcernConfig.Default()));
*///        System.out.println("Number of segments after filtering target routes : " + filteredIncidents.count());
//        JavaDStream<Detectors> processedData = filteredIncidents.filter(cleanData());
        //Pushing raw data to mongodb
//        JavaDStream<Document> rawData = processedData.map(convertRawDataToMongoDocument());
//        saveIncidentsToMongo(rawData, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForRawInrixData(),
//                scala.Option.apply(config.getMongoConnectionString()),
//                false, 500, 0, WriteConcernConfig.Default()));

        //Consider segments with cvalue>=30, score=30 length of the segment > 0.2
//        JavaDStream<Detectors> filteredIncidents2 = filteredIncidents.filter(filterByCvalueAndScoreAndMiles());
//        System.out.println("Number of segments after filtering miles > 0.2 : " + filteredIncidents2.count());
        //Find the potential incidents
        JavaPairDStream<Incident, Detectors> incidentAndDataMap = filteredIncidents.mapToPair(mapToIncidents2(Long.valueOf(config.getIncidentsOccurredInPastInMinutes())));
        //filter the potential incidents with incident tag as "INCIDENT" and update the ones' with incident OVER
        JavaPairDStream<Incident, Detectors> potentialIncidents = incidentAndDataMap.filter(filterPotentialIncidents());
        System.out.println(potentialIncidents.count());
        //Convert incidents to mongo document
        JavaPairDStream<Incident, Document> mapIncidents = potentialIncidents.mapToPair(convertToMongoDoc());
//        JavaDStream<Document> documents = potentialIncidents.map(convertToMongoDocument2());
        //Save the incidents
//        saveIncidentsToMongo(documents, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForProcessedInrixData(),
//                scala.Option.apply(config.getMongoConnectionString()),
//                false, 500, 0, WriteConcernConfig.Default()));
        saveIncidentsToMongo2(mapIncidents, new WriteConfig(config.getMongoDbName(), config.getMongoCollectionNameForProcessedInrixData(),
                scala.Option.apply(config.getMongoConnectionString()),
                false, 500, 0, WriteConcernConfig.Default()));

    }

 /*   private static Function<InrixData, Boolean> cleanData() {
        return new Function<InrixData, Boolean>() {
            @Override
            public Boolean call(InrixData inrixDataObj) throws Exception {
                LocalDateTime datetime = getDateTime(inrixDataObj.getTimestamp());
                String code = inrixDataObj.getCode();
                String startlat = jedis.hget(code, "startlat");
                String distance = jedis.hget(code, "distance");

                if (inrixDataObj.getScore().length() < 1 ||
                        inrixDataObj.getCvalue().length() < 1 || inrixDataObj.getSpeed().isEmpty()
                        || datetime == null || distance == null || startlat == null) {
                    return false;
                }
                return true;
            }
        };
    }*/

//    private static Function<InrixData, Document> convertRawDataToMongoDocument() {
//        return new Function<InrixData, Document>() {
//            @Override
//            public Document call(InrixData inrixData) throws Exception {
//                String code = inrixData.getCode();
//                String startTimestamp = inrixData.getTimestamp();
//                double thresholdSpeed = getThresholdSpeed(startTimestamp, code, jedis);
//
//                LocationData locationData = JedisUtils.retrieveLocationDataFromJedis(jedis, code);
//                final String finalInrixString = code + "," + thresholdSpeed
//                        + "," + inrixData.getSpeed() + "," + inrixData.getAverage() + "," + "Inrix" + ","
//                        + locationData.getFid() + "," + locationData.getOid_1() + "," + locationData.getPrevious_code() + "," + locationData.getNext_code() + "," + locationData.getFrc() + "," + locationData.getRoadnumber() + "," + locationData.getRoadname() + ","
//                        + locationData.getLinearid() + "," + locationData.getCountry() + "," + locationData.getState() + "," + locationData.getCounty() + "," + locationData.getDistrict() + "," + locationData.getMiles() + "," + locationData.getSliproad() + "," +
//                        locationData.getSpecialroad() + "," + locationData.getRoadlist() + "," + locationData.getStartlat() + "," + locationData.getStartlong() + "," + locationData.getEndlat() + "," + locationData.getEndlong() + "," +
//                        locationData.getBearing() + "," + locationData.getXdgroup() + "," + locationData.getShapesrid();
//                String segmentLocationDataCsvToJson = CsvToJsonConvertor.rawDataCsvToJson(finalInrixString);
//                Document doc = Document.parse(segmentLocationDataCsvToJson);
//                return doc;
//            }
//        };
//    }

    private static Function<String, Detectors> convertToDetectorsObject() {
        return new Function<String, Detectors>() {
            @Override
            public Detectors call(String s) throws Exception {
                return csvToDetectorsData(s);
            }
        };
    }

    private static Function<Detectors, Boolean> filterDefectedSensors() {
        return new Function<Detectors, Boolean>() {
            @Override
            public Boolean call(Detectors detectors) {
                if (detectors != null) {
                    if (detectors.getStatus().equals("operational")) {
                        return true;
                    }
                }
                        return false;
            }
        };
    }


    public static PairFunction<Detectors, Incident, Detectors> mapToIncidents2(long incidentsOccurredInPastInMinutes) {
        return new PairFunction<Detectors, Incident, Detectors>() {
            @Override
            public Tuple2<Incident, Detectors> call(Detectors detectors) throws Exception {
                double speed = Double.parseDouble(detectors.getSmoothedSpeed());
                String detectorId = detectors.getDetectorId();
                String startTimestamp = detectors.getLocalDate().trim() + "_" + detectors.getStarttime().trim();
                double thresholdSpeed = getThresholdSpeed(startTimestamp, detectorId, jedis);
//                int score = Integer.parseInt(inrixDataObj.getScore());
//                int cvalue = Integer.parseInt(inrixDataObj.getCvalue());
                Incident potentialIncident = getPotentialIncident(detectorId, speed, thresholdSpeed, startTimestamp, incidentsOccurredInPastInMinutes);
                return new Tuple2<Incident, Detectors>(potentialIncident, detectors);
            }
        };
    }

    private static Function<Tuple2<Incident, Detectors>, Boolean> filterPotentialIncidents() {
        return new Function<Tuple2<Incident, Detectors>, Boolean>() {
            @Override
            public Boolean call(Tuple2<Incident, Detectors> potentialIncident) throws Exception {
                String detectorId = potentialIncident._2.getDetectorId();
                String incident = potentialIncident._1.getIncident();
                if (incident.equals("INCIDENT")) {
                    return true;
                } else if (incident.equals("OVER")) {
                    detectorToEventId.remove(detectorId);
                    String endOfIncidentTimeStamp = potentialIncident._1.getEndTimestamp();
                    boolean updatedSuccessfully = MongoUpdate.update(inrixDataCollection, detectorId, endOfIncidentTimeStamp, incident);
                    if (updatedSuccessfully) {
                        System.out.println("Incident " + detectorId + " is over : " + endOfIncidentTimeStamp);
                    } else {
                        System.out.println("INCIDENT IS OVER BUT NOT UPDATED!!!!!! " + detectorId);
                    }
                }
                return false;
            }
        };
    }

    private static Function<Detectors, Boolean> filterOnlyIncidentSegments() {
        return new Function<Detectors, Boolean>() {
            @Override
            public Boolean call(Detectors inrixDataObj) throws Exception {
                if (inrixDataObj != null) {
                    String detectorId = inrixDataObj.getDetectorId();
                    if (detectors.contains(detectorId)) {
                        return true;
                    }
                }
                return false;
            }
        };
    }

    private static Detectors csvToDetectorsData(String csvRecord) throws IOException {
        String splitBy = ",";
        String[] detectorsArray = csvRecord.split(splitBy);
        if (detectorsArray.length == 13) {
            return new Detectors(detectorsArray[0], detectorsArray[1], detectorsArray[2],
                    detectorsArray[3], detectorsArray[4], detectorsArray[5], detectorsArray[6],
                    detectorsArray[7], detectorsArray[8], detectorsArray[9], detectorsArray[10], detectorsArray[11], detectorsArray[12]);
        }
        return null;

    }

    private static double getThresholdSpeed(String startTimestamp, String detectorId, Jedis jedis) {
        double thresholdSpeed;
        LocalDateTime datetime = getDateTime(startTimestamp);
        if (datetime == null) return 0.0;
        //in java Monday = 1 and Sunday = 7. Do mod 7 to match with the
        //datafile inrix_param_may31.csv where sunday=0 and saturday=6
        int weekday = 0;
        int hour = 0;
        int min = 0;
        if (startTimestamp != null) {
            weekday = datetime.getDayOfWeek().getValue() % 7;
            hour = datetime.getHour();
            min = datetime.getMinute();
        }
        double period = getPeriod(min);
        String key = Utils.genKey(detectorId, weekday, hour);
        String tspeed = jedis.hget(key, String.valueOf(period));
        if (tspeed != null)
            thresholdSpeed = Double.parseDouble(tspeed);
        else
            thresholdSpeed = 0.0;    //This is an workaround. Need to correct later
        return thresholdSpeed;
    }

    private static double getPeriod(int min) {
        double period = 0.0;

        if ((min >= 0) && (min <= 14))
            period = 0.0;
        else if ((min >= 15) && (min <= 29))
            period = 1.0;
        else if ((min >= 30) && (min <= 44))
            period = 2.0;
        else if ((min >= 45) && (min <= 59))
            period = 3.0;

        return period;
    }

    private static LocalDateTime getDateTime(String startTimestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
        LocalDateTime datetime = null;
        try {
            datetime = LocalDateTime.parse(startTimestamp, formatter);
        } catch (Exception e) {
            System.out.println("cannot parse date");
        }
        return datetime;
    }

    private static PairFunction<Tuple2<Incident,Detectors>, Incident, Document> convertToMongoDoc() {
        return new PairFunction<Tuple2<Incident,Detectors>, Incident, Document>() {
            @Override
            public Tuple2<Incident, Document> call(Tuple2<Incident, Detectors> incidentInrixDataTuple2) throws Exception {

                Detectors detectors = incidentInrixDataTuple2._2;
                String detectorId = incidentInrixDataTuple2._2.getDetectorId();
                LocationData locationData = JedisUtils.retrieveLocationDataFromJedis(jedis, detectorId);
                System.out.println(locationData);

                Incident incidentDetails = incidentInrixDataTuple2._1;
                final String finalInrixString = incidentDetails.getEventId() + "," + incidentDetails.getStartTS() + "," + null + "," + "0.0"
                        + "," + detectors.getSmoothedSpeed() + "," + detectors.getAvgSpeed() + "," + "Wavetronix" + "," + Boolean.parseBoolean("false") + "," + incidentDetails.getIncidentCount() + "," + incidentDetails.getCurrentIncident() + "," + detectorId + ","
                        + locationData.getOrganization_Id() + "," + locationData.getNetwork_Id() + "," + locationData.getLocal_date() + "," + locationData.getLocal_time() + "," + locationData.getUtc_offset() + "," + locationData.getStation_Id() + "," + locationData.getDetector_name() + ","
                        + locationData.getLatitude() + "," + locationData.getLongitude() + "," + locationData.getLink_ownership() + "," + locationData.getRoute_designator() + "," + locationData.getLinear_reference() + "," + locationData.getDetector_type() + "," + locationData.getApproach_direction() + "," +
                        locationData.getApproach_name() + "," + locationData.getLanes_type() + "," + locationData.getLane_id() + "," + locationData.getLane_name() + "," + incidentDetails.isDuplicate();
                String segmentLocationDataCsvToJson = CsvToJsonConvertor.segmentLocationDataCsvToJson(finalInrixString);

                Document doc = Document.parse(segmentLocationDataCsvToJson);
                return new Tuple2<>(incidentInrixDataTuple2._1, doc);

            }
        };
    }

    //incident States: IGNORE, POTENTIAL, INCIDENT, IN_PROGRESS, MAY_BE_OVER, OVER
    private static Incident getPotentialIncident(String detectorId, double speed, double thresholdSpeed, String startTimestamp,
                                                 long incidentsOccurredInPastInMinutes) {
        Incident incident = detectorToIncident.get(detectorId);
        if (speed < thresholdSpeed && speed < 45.0) {
            incident = updateIncidentCount(incident, detectorId, startTimestamp, incidentsOccurredInPastInMinutes);
        } else if (detectorToIncident.containsKey(detectorId) && !incident.getIncident().equals("IGNORE")) {
            incident = updateIncidentOverCount(incident, detectorId, incidentsOccurredInPastInMinutes);
        } else {
            incident = new Incident(startTimestamp, "IGNORE",
                    0, 0, 0, detectorId, "Not an Event", null, false);
        }
        detectorToIncident.put(detectorId, incident);
        return incident;
    }

    private static Incident updateIncidentCount(Incident incident, String detectorId, String startTimestamp, long incidentsOccurredInPastInMinutes) {
        if (detectorToIncident.containsKey(detectorId)) {
            int incidentCount = incident.getIncidentCount();
            String incidentState = incident.getIncident();
            incidentCount++;
            if (incidentCount < 3) {
                incident.setIncidentCount(incidentCount);
                incident.setIncident("POTENTIAL");
                incident.setOverCount(0);
                incident.setCurrentIncident(0);
                incident.setEventId("Not an event");
                System.out.println("POTENTIAL : " + incident);
            } else if (incidentCount == 3 && incidentState.equals("POTENTIAL")) {
                incident.setIncidentCount(incidentCount);
                incident.setOverCount(0);
                incident.setCurrentIncident(1);
                incident.setIncident("INCIDENT");
                Pair<String, Boolean> eventIDDuplicate = generateEventId(incident.getIncident(), detectorId, incidentsOccurredInPastInMinutes, incident);
                incident.setEventId(eventIDDuplicate.getKey());
                incident.setDuplicate(eventIDDuplicate.getValue());
                System.out.println("INCIDENT : " + incident);
            } else {
                incident.setIncidentCount(incidentCount);
                incident.setOverCount(0);
                incident.setCurrentIncident(1);
                incident.setIncident("IN_PROGRESS");
                System.out.println("IN PROGRESS : " + incident);
            }
        } else {
            incident = new Incident(startTimestamp, "POTENTIAL",
                    0, 1, 0, detectorId, "Not an Event", null, false);
            System.out.println("POTENTIAL : " + incident);
        }
        return incident;
    }

    private static Incident updateIncidentOverCount(Incident incident, String detectorId, long incidentsOccurredInPastInMinutes) {
        int incidentCount = incident.getIncidentCount();
        String incidentState = incident.getIncident();
        int overCount = incident.getOverCount();
        if (incidentCount < 3) {
            incident.setCurrentIncident(0);
            incident.setIncident("IGNORE");
            incident.setOverCount(0);
            incident.setIncidentCount(0);
            incident.setEventId("Not an Event");
            incident.setEndTimestamp(null);
            System.out.println("IGNORE after POTENTIAL : " + incident);
        } else if (overCount < 5) {
            incident.setOverCount(overCount + 1);
            incident.setIncident("MAY_BE_OVER");
            incident.setCurrentIncident(1);
            incident.setIncidentCount(incidentCount);
            System.out.println("MAY Be OVER : " + incident);
        } else {
            incident.setOverCount(overCount);
            incident.setIncident("OVER");
            incident.setCurrentIncident(0);
            incident.setIncidentCount(0);
            Pair<String, Boolean> eventIDAndDuplicate = generateEventId("OVER", detectorId, incidentsOccurredInPastInMinutes, incident);
            incident.setEventId(eventIDAndDuplicate.getKey());
            incident.setEndTimestamp(new Date(System.currentTimeMillis()).toString());
            System.out.println("OVER : " + incident);
        }
        return incident;
    }

    private static Pair<String, Boolean> generateEventId(String incident, String detectorId, long incidentsOccurredInPastInMinutes, Incident incidentObj) {
        boolean duplicate = true;
        String eventId = "Not an event";
        if (incident.equals("OVER")) {
            eventId = detectorToEventId.get(detectorId);
            detectorToEventId.remove(detectorId);
        } else if (incident.equals("INCIDENT")) {
            String key = detectorId + "L";
            long count = jedis.llen(key);
            List<String> detectors = jedis.lrange(key, 0, count - 1);
            eventId = null;
            //Check if any incidents are in progress within 2 miles
//            System.out.println("List of detectors within 2 miles: " + detectors);
            for (String detector : detectors) {
                if (detectorToEventId.get(detector) != null) {
                    eventId = detectorToEventId.get(detector);
                    detectorToEventId.put(detectorId, eventId);
                    break;
                }
            }
            //Check if any incidents had occured within 2 miles and within last few minutes
            if (eventId == null) {
                Map<String, String> oldIncidents = loadOldIncidents(inrixDataCollection, incidentsOccurredInPastInMinutes);
                System.out.println("Old Incidents - " + oldIncidents);
                List<String> list = detectors.stream().filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) {
                        return oldIncidents.containsKey(s);
                    }
                }).collect(Collectors.toList());
                if (!list.isEmpty()) {
                    eventId = oldIncidents.get(list.get(0));
                }

            }

            if (eventId == null) {
                duplicate = false;
                id++;
                Date today = new Date();
                DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
                String dateString = df2.format(today);
                eventId = dateString + "_" + System.currentTimeMillis() + "_" + detectorId;
//                eventId = dateString + "-" + id;
                detectorToEventId.put(detectorId, eventId);
            }
//            incidentObj.setDuplicate(duplicate);
        }

        return new Pair<>(eventId, duplicate);
    }

    private static Map<String, String> loadOldIncidents(MongoCollection<Document> inrixDataCollection, long minutes) {
        Map<String, String> oldCodeToEventID = new HashMap<>();
        Date startDate = new Date(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(minutes));

        for (Document doc : MongoUpdate.getOldIncidents(inrixDataCollection, startDate)) {
            oldCodeToEventID.put(String.valueOf(doc.get("detector_Id")), String.valueOf(doc.get("event_id")));

        }
        return oldCodeToEventID;
    }

    private static void saveIncidentsToMongo(JavaDStream<Document> incidents, WriteConfig wc_incident) {
        incidents.foreachRDD(new VoidFunction<JavaRDD<Document>>() {
            @Override
            public void call(JavaRDD<Document> documentJavaRDD) throws Exception {
//                System.out.println("Saving incidents "+ documentJavaRDD.count());

                MongoSpark.save(documentJavaRDD, wc_incident);
            }
        });

    }

    private static void saveIncidentsToMongo2(JavaPairDStream<Incident, Document> incidents, WriteConfig wc_incident) {
        List<String> removeDetectors = new ArrayList<>();
        incidents.foreachRDD(new VoidFunction<JavaPairRDD<Incident, Document>>() {
            @Override
            public void call(JavaPairRDD<Incident, Document> incidentsRDD) throws Exception {

                JavaRDD<Incident> keys = incidentsRDD.keys();
                incidentsRDD.foreach(new VoidFunction<Tuple2<Incident, Document>>() {
                    @Override
                    public void call(Tuple2<Incident, Document> incidentDocumentTuple2) throws Exception {
                        boolean duplicate = false;
                        String detectorId =incidentDocumentTuple2._1.getCode();
                        String incidentStr = incidentDocumentTuple2._1.getIncident();
                        if (incidentStr.equals("INCIDENT")) {
                            String key = detectorId + "L";
                            long count = jedis.llen(key);
                            List<String> detectors = jedis.lrange(key, 0, count - 1);
                            String originalEventId = detectorToEventId.get(detectorId);
                            for (String detector : detectors) {
                                if (detectorToEventId.containsKey(detector)) {
                                    String eventId = detectorToEventId.get(detector);
                                    if (eventId.equals(originalEventId))
                                        continue;
                                    removeDetectors.add(detectorId);
                                    duplicate = true;
                                    detectorToEventId.put(detectorId, eventId);
//                                    detectorToIncident.remove(code);
                                    System.out.println("Removed duplicate event which is within 2 miles " + detectorId);
                                    System.out.println("Near by code is " + detector);
                                    break;
                                }
                            }
                        }
                        boolean dup = incidentDocumentTuple2._2.getBoolean("duplicate");
                        MongoUpdate.saveIncident(inrixDataCollection, incidentDocumentTuple2._2);
                        MongoUpdate.updateStartTimeStamp(inrixDataCollection, incidentDocumentTuple2._2);
                        if (duplicate || dup) {
                            MongoUpdate.updateDuplicate(inrixDataCollection, incidentDocumentTuple2._2);
                        }
                    }
                });
            }
        });




    }
}
