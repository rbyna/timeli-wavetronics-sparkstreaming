package com.timeli.wavetronix.sparkstreaming.utils;

import com.timeli.wavetronix.sparkstreaming.obj.LocationData;
import redis.clients.jedis.Jedis;

public class JedisUtils {

    public static LocationData retrieveLocationDataFromJedis(Jedis jedis, String detectorId) {

        return new LocationData(detectorId, jedis.hget(detectorId, "organization_Id"), jedis.hget(detectorId, "network_Id"),
                jedis.hget(detectorId, "local_date"), jedis.hget(detectorId, "local_time"), jedis.hget(detectorId, "utc_offset"),
                jedis.hget(detectorId, "station_Id"), jedis.hget(detectorId, "detector_name"), jedis.hget(detectorId, "latitude"),
                jedis.hget(detectorId, "longitude"), jedis.hget(detectorId, "link_ownership"), jedis.hget(detectorId, "route_designator"),
                jedis.hget(detectorId, "linear_reference"), jedis.hget(detectorId, "detector_type"), jedis.hget(detectorId, "approach_direction"),
                jedis.hget(detectorId, "approach_name"), jedis.hget(detectorId, "lanes_type"), jedis.hget(detectorId, "lane_id"),
                jedis.hget(detectorId, "lane_name"));
    }
}
